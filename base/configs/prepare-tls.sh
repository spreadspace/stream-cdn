set -e
chmod 0750 /srv/tls
cp "/var/lib/acme/live/$TLS_CERT_NAME.$TLS_CERT_DOMAIN/fullchain" /srv/tls
cp "/var/lib/acme/live/$TLS_CERT_NAME.$TLS_CERT_DOMAIN/privkey" /srv/tls
chmod g+r /srv/tls/privkey
chown -R 0:990 /srv/tls
echo "successfully copied certificate: $TLS_CERT_NAME.$TLS_CERT_DOMAIN"
