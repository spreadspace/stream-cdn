#!/bin/bash

for node in ele-telesto; do
  kubectl label --overwrite node "$node" streaming.spreadspace.org/zone=source
done

kubectl label --overwrite node "emc-dist0" streaming.spreadspace.org/zone=dist-root
# for idx in $(seq 1 x); do
#   kubectl label --overwrite node "emc-dist$idx" streaming.spreadspace.org/zone=dist-level1
# done

for idx in $(seq -w 01 03); do
  kubectl label --overwrite node "emc-$idx" streaming.spreadspace.org/zone=dist-leaf
done

kubectl label --overwrite node "emc-00" streaming.spreadspace.org/zone=dist-lb
